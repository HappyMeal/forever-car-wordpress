<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ForeverCar');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ']{V|mLB`pQL|9LVp%O*]_M)X[uZo$&]FmJRd=mD@QD`JETs&};(=e&Icz|=Nqn2n');
define('SECURE_AUTH_KEY',  'Qd,NSm`)oaC],e|;>.+Nm57sGQB$-:F:l}+T/Rg-RW-E2YC;X+f]MkNyn4qP?`Xj');
define('LOGGED_IN_KEY',    'xq4} KSR+)S[lkp*&:m)e&8)Mch0*huFUo;0K[*;)UM/ 9{>hb*RX,r&tzFv=K#0');
define('NONCE_KEY',        'AvO-_gQk0r{i|:^G:pz@kZM!*|[+<#zA;N[-1|KQ=J^|x36p*uV+4zLoy(YEe32N');
define('AUTH_SALT',        '8u4Ek5s<v%?I0}D,8g2<G*[8IvAwMr(P+m#?sK%lwXb`c|UoMfI~B|/):i8wO;N(');
define('SECURE_AUTH_SALT', 'pMA6^5u!;!2PW;|u[4.6 0Q)>Kr%0BCiyDh@h+Tnwt^|y3f3pB+|*6rd<|Cz5y<S');
define('LOGGED_IN_SALT',   'K[HM0ek:q?OKF5h 0j+[}_okz1&aF%}:(2gE3IYi;!p$~:+-X$78$fLz-q@_Y*]P');
define('NONCE_SALT',       '$t`xWY.dHM|-Gt-0qS~zG<]7F_`]Ll(-io|CDcr7iB*%[M1#Z|]F3EBDnp~6|A|y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
