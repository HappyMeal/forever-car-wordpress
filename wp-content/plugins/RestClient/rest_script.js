jQuery(document).ready( function() {

    //signin submit
    jQuery('#sign-in-submit').on( 'click', function( e ) {

        e.preventDefault();
        console.log( 'hola' );

        var data = { action: "submit_request",
                     func: jQuery(this).siblings('#action').val(),
                     method: jQuery(this).siblings('#method').val(),
                     username: jQuery(this).siblings('#username').val(),
                     password: jQuery(this).siblings('#password').val(),
                   };

        jQuery.ajax({
            type     : "post",
            dataType : "json",
            url      : myAjax.ajaxurl,
            data     : data,
            success  : function(response) {

                console.log( 'success' );
                console.log( response );
                jQuery( '.form-submission-results' ).html( response.login );
            },

            error: function( XMLHttpRequest, textStatus, errorThrown ) {

                console.log( errorThrown );
                console.log( XMLHttpRequest );

            }

        }); //end ajax

    });//end sign in click

    //get quote submit
    jQuery('#get-quote-submit').on( 'click', function( e ) {

        e.preventDefault();
        console.log( 'hola' );

        var data = { action: "submit_request",
                     func: jQuery(this).siblings('#action').val(),
                     method: jQuery(this).siblings('#method').val(),
                     vin: jQuery(this).siblings('#vin').val(),
                     make: jQuery(this).siblings('#make').val(),
                     model: jQuery(this).siblings('#model').val(),
                     sort_by: jQuery(this).siblings('#sort-preference').val(),
                   };

        jQuery.ajax({
            type     : "post",
            dataType : "json",
            url      : myAjax.ajaxurl,
            data     : data,
            success  : function(response) {

                console.log( 'success' );

                var html_response_first  = '<h3>Recommended for You</h3>';
                var html_response_second = '<h3>More Options</h3>';
                var html_response_more   = '';

                jQuery.each( response, function( index, value ) {

                  var plan_id = value.plan_code + '-' + value.plan_term_months + '-' + value.plan_term_miles;

                  var temp_string = '<div class="plan-container"><strong>$' + value.plan_term_dealer_cost + '</strong> ' + value.plan_term_months + ' months, ' + value.plan_term_miles + ' miles' + '<a href="#" class="show-details">&nbsp;details ></a><div class="plan-details">Plan Code: ' + value.plan_code + '<br />Deductible: $' + value.plan_term_deductible + '<br />Deductible Type: ' + value.plan_term_deductible_type + '<br />Vendor: ' + value.plan_provider + '<br /><a href="#" class="choose-plan">Choose this plan</a><br /><a href="#" class="save-plan">Save this plan for later</a><br /><a href="#" class="compare-plan">Compare this plan</a><input type="hidden" class="plan-id" value="' + plan_id + '"/></div></div>';

                  if( index < 3 ) {

                    html_response_first += temp_string;

                  } else if( index < 10 ) {

                    html_response_second += temp_string;

                  } else {

                    html_response_more += temp_string;

                  }//end else



                });//end each

                jQuery( '.form-submission-results' ).html( html_response_first + html_response_second );
                jQuery( '.form-submission-results-more' ).html( html_response_more );

            },

            error: function( XMLHttpRequest, textStatus, errorThrown ) {

                console.log( errorThrown );
                console.log( XMLHttpRequest );

            }

        }); //end ajax

    });//end get quote in click

    //payment submit
    jQuery('#submit-payment').on( 'click', function( e ) {

        e.preventDefault();
        console.log( 'hola submit payment' );

        if( validateCardNumber(  jQuery(this).siblings('#cc_number').val() ) == false ) {
          alert( 'Invalid CC Number' );
          return false;
        }

        var data = { action: "submit_request",
                     func: jQuery(this).siblings('#action').val(),
                     method: jQuery(this).siblings('#method').val(),
                     cc_number: jQuery(this).siblings('#cc_number').val(),
                     cc_type: jQuery(this).siblings('#cc_type').val(),
                     cc_first_name: jQuery(this).siblings('#cc_first_name').val(),
                     cc_last_name: jQuery(this).siblings('#cc_last_name').val(),
                     cc_csc_code: jQuery(this).siblings('#cc_csc_code').val(),
                     quote_id: jQuery(this).siblings('#quote_id').val(),
                   };

        jQuery.ajax({
            type: "post",
            dataType: "json",
            url: myAjax.ajaxurl,
            data: data,
            success  : function(response) {

                console.log( 'success' );
                console.log( response );

                var confirm_order_html = '<div>Payment Received: ' + response.payment_received + '<br />Name: '+ response.cc_first_name + ' ' + response.cc_last_name + '</div>';

                jQuery( '.confirm-order' ).html( confirm_order_html );

            },

            error: function( XMLHttpRequest, textStatus, errorThrown ) {

                console.log( errorThrown );
                console.log( XMLHttpRequest );

            }

        }); //end ajax

    });//end payment submit

    //get quote submit
    jQuery('#submit-search').on( 'click', function( e ) {

        e.preventDefault();
        console.log( 'hola from submit search' );

        var data = { action: 'submit_request',
                     func: jQuery(this).siblings('#action').val(),
                     method: jQuery(this).siblings('#method').val(),
                     min_mileage: jQuery(this).siblings('#filter-minimum-mileage').val(),
                     max_mileage: jQuery(this).siblings('#filter-maximum-mileage').val(),
                     max_price: jQuery(this).siblings('#filter-maximum-price').val(),
                   };

        jQuery.ajax({
            type     : "post",
            dataType : "json",
            url      : myAjax.ajaxurl,
            data     : data,
            success  : function(response) {

                console.log( 'success' );
                console.log( response );

                var html_text = '';

                if( response == 'null' ) {

                  html_text = 'Sorry - no plan found.';

                } else {

                  jQuery.each( response, function( key, value ){

                    html_text += "<br />====<br />PLAN CODE: " + value.plan_code + "<br />MONTHS: " + value.plan_term_months + "<br />MILES: " + value.plan_term_miles + "<br />PRICE: " + value.plan_term_dealer_cost;

                  });
                }

                jQuery( '.search-results' ).html( html_text );

            },

            error: function( XMLHttpRequest, textStatus, errorThrown ) {

                console.log( errorThrown );
                console.log( XMLHttpRequest );

            }

        }); //end ajax

    });//end get quote in click

    jQuery('a.show-more-results').on('click', function( e ){

      e.preventDefault();
      jQuery('.form-submission-results-more').slideToggle();
      return false;

    });//end click show more

    //choose plan ( add to cart )
    jQuery('#primary.content-area').on( 'click', '.choose-plan', function( e ){

      e.preventDefault();
      console.log( 'add to cart: ' + jQuery(this).nextAll('.plan-id').val() );
      return false;

    });

    //save plan for later
    jQuery('#primary.content-area').on( 'click', '.save-plan', function( e ){

      e.preventDefault();
      console.log( 'save this plan: ' + jQuery(this).nextAll('.plan-id').val() );
      return false;

    });

    //compare plan to others
    jQuery('#primary.content-area').on( 'click', '.compare-plan', function( e ){

      e.preventDefault();
      console.log( 'compare this plan: ' + jQuery(this).nextAll('.plan-id').val() );
      return false;

    });

    //show details
    jQuery('#primary.content-area').on( 'click', '.show-details', function( e ){

      e.preventDefault();
      jQuery(this).next('.plan-details').slideToggle();
      return false;

    });

}); //end document ready

function validateCardNumber(number) {
    var regex = new RegExp("^[0-9]{16}$");
    if (!regex.test(number))
        return false;

    return luhnCheck(number);
}

function luhnCheck(val) {
    var sum = 0;
    for (var i = 0; i < val.length; i++) {
        var intVal = parseInt(val.substr(i, 1));
        if (i % 2 == 0) {
            intVal *= 2;
            if (intVal > 9) {
                intVal = 1 + (intVal % 10);
            }
        }
        sum += intVal;
    }
    return (sum % 10) == 0;
}
