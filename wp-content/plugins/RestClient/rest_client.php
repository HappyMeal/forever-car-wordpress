<?php

    //prevent direct access
    defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

    /**
     * Plugin Name: Rest Client
     * Plugin URI: http://www.honeymanghost.com
     * Description: provides a REST client
     * Version: 1.0.0
     * Author: Karim Elghobashi
     * Author URI: http://www.honeymanghost.com
     * License: A short license name. Example: GPL2
     */

    /*_________________________ FUNCTIONS _____________________________*/
    //add js
    add_action( 'init', 'custom_script_enqueuer' );

    function custom_script_enqueuer() {
       wp_register_script( 'rest_script', WP_PLUGIN_URL . '/RestClient/rest_script.js', array('jquery') );
       wp_localize_script( 'rest_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));

       wp_enqueue_script( 'jquery' );
       wp_enqueue_script( 'rest_script' );

    }//end script enqueuer

    //add ajax callbacks - for admin and logged out
    add_action( 'wp_ajax_submit_request', 'submit_request' );
    add_action( 'wp_ajax_nopriv_submit_request', 'submit_request' );

    /*
     *========================= BEGIN CLASS ========================
     *___________________________________________________________________*/
    class RestClient{

        //members
        private $url;
        private $django_url;
        private $vin_array;

        //constructor
        function __construct() {

            error_log( 'Hello From Rest Client' );

            $this->url        = 'http://angular_rest.local:8888/server/forever.php';
            $this->django_url = 'http://127.0.0.1:8000/api/get_recommendations/yolo2/';
            $this->vin_array  = array(
                'A' => 2010,
                'B' => 2011,
                'C' => 2012,
                'D' => 2013,
                'E' => 2014,
                'F' => 2015,
                'G' => 2016,
                'H' => 2017,
                'J' => 2018,
                'K' => 2019,
                'L' => 1990,
                'M' => 1991,
                'N' => 1992,
                'P' => 1993,
                'R' => 1994,
                'S' => 1995,
                'T' => 1996,
                'V' => 1997,
                'W' => 1998,
                'X' => 1999,
                'Y' => 2000,
                '1' => 2001,
                '2' => 2002,
                '3' => 2003,
                '4' => 2004,
                '5' => 2005,
                '6' => 2006,
                '7' => 2007,
                '8' => 2008,
                '9' => 2009,
            );

        }//end constructor

        /*
         * private methods
         * ________________________________________________________________*/

        /*
         * make all requests
         * optional $django_url - hits live API if TRUE, otherwise local test API
         *
         */
        private function make_request( $data, $django_url = FALSE ) {

            $ch           = curl_init();
            $query_string = isset( $data['query_string'] ) ? $data['query_string'] : '';

            if( $django_url ) {

              curl_setopt( $ch, CURLOPT_URL, $this->django_url . $query_string );
              curl_setopt( $ch, CURLOPT_POST, false );
              curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
              curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, 'GET' );

            } else {

              curl_setopt( $ch, CURLOPT_URL, $this->url . '?action=' . $data['action'] . $query_string );
              curl_setopt( $ch, CURLOPT_POST, true );
              curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true);
              curl_setopt( $ch, CURLOPT_CUSTOMREQUEST, $data['package']['method'] );
              curl_setopt( $ch, CURLOPT_POSTFIELDS, $data['package'] );

            }

            $result   = curl_exec( $ch );
            $httpCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

            //store quote_id in $_SESSION
            if( $data['action'] == 'login' ) {

                $this->store_user_id( json_decode( $result )->username );

            }//end if

            return $result;

            curl_close( $ch );

        }//end make repuest

        //create / update session variable
        private function store_user_id( $user_id ) {

            session_start();
            $_SESSION['user_id'] = $user_id;

        }//end save_quote_id

        /*
         * public helpers
         * ______________________________________________________________*/
         public function get_year_from_vin( $char ) {

             return $this->vin_array[ $char ];

         }

        public function get_quote( $data ) {

            $data['action'] = 'get_quote';

            /*
            $data['query_string'] = isset( $data['package']['vin'] ) && $data['package']['vin'] != '' ? '&vin=' . $data['package']['vin'] :
            '&make=' . $data['package']['make'] . '&model=' . $data['package']['model'] . '&year=' . $data['package']['mnf_date'];
            $data['query_string'] .= '&mileage=' . $data['package']['mileage']. '&state=' . $data['package']['state'] . '&phone=' . $data['package']['phone'] . '&sort_by=' . $data['package']['sort_by'];
            */

            $data['query_string'] = '?vin=' . $data['package']['vin'] . '&sort_by=' . $data['package']['sort_by'];

            return $this->make_request( $data, TRUE );

        }//end get_account

        public function login( $data ) {

            $data['action']       = 'login';
            $data['query_string'] = '&username=' . $data['package']['username'] . '&password=' . $data['package']['password'];
            return $this->make_request( $data );

        }//end login

        public function submit_payment( $data ) {

            $data['action']       = 'submit_payment';
            $data['query_string'] = '&cc_number=' . $data['package']['cc_number'] . '&cc_type=' . $data['package']['cc_type'] . '&cc_first_name=' . $data['package']['cc_first_name'] .'&cc_last_name=' . $data['package']['cc_last_name'] .  '&cc_csc_code=' . $data['package']['cc_csc_code'] . '&quote_id=' . $data['package']['quote_id'];
            return $this->make_request( $data );

        }//end save quote

        public function get_account_info( $user_id ) {

          $data['action']       = 'get_account_info';
          $data['query_string'] = '&user_id=' . $user_id;
          return $this->make_request( $data );

        }//end get_account_info

        public function search( $data ) {

          $data['action'] = 'search';
          $all_plans      = $this->make_request( $data, TRUE );
          $filtered_plans = array();

          foreach( json_decode( $all_plans ) as $plan ) {

            //error_log( $plan->plan_term_miles . $data['mileage'] );

            if( $plan->plan_term_miles >= $data['package']['min_mileage'] &&
                $plan->plan_term_miles <= $data['package']['max_mileage'] &&
                $plan->plan_term_dealer_cost <= $data['package']['max_price'] ) {

              $filtered_plans[] = $plan;

            }//end if

          }//end foreach

          echo json_encode( $filtered_plans );

        }//end get_account_info

    }
    /*
     *========================= END CLASS ==========================
     *___________________________________________________________________*/


    /*
     *=================== BEGIN AJAX CALLBACKS =====================
     *___________________________________________________________________*/

    //all submit events
    function submit_request() {

        $rc = new RestClient();

        error_log( 'inside ajax submit' );
        error_log( serialize( $_POST ) );

        if( isset( $_POST['func'] ) && $_POST['func'] != '' ) {

            error_log( 'Processing this request ' . $_POST['func'] );

            echo $rc->$_POST['func']( array( 'package' => $_POST ) );

        }//end if

        //prevent extra return data
        wp_die();

    }//end submit request

?>
