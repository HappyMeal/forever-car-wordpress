<?php

    /**
     * The form page template file
     *
     * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
     *
     * @package WordPress
     * @subpackage Twenty_Fifteen
     * @since Twenty Fifteen 1.0
     */
    session_start();
    get_header();

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

            <?php

                echo '<div class="current-quote">CURRENT USER ID: <span>' . $_SESSION['user_id'] . '</span></div>';

            ?>

            <form action="/form/" method="post">
                <h3>SIGN IN</h3>
                <input type="hidden" name="action" id="action" value="login" />
                <input type="hidden" name="method" id="method" value="get" />
                <input type="text" placeholder="username" name="username" id="username" />
                <input type="password" placeholder="password" name="password" id="password" />
                <input type="submit" id="sign-in-submit" />
            </form>

            <form action="/form/" method="post">
                <h3>GET QUOTE</h3>
                <input type="hidden" name="action" id="action" value="get_quote" />
                <input type="hidden" name="method" id="method" value="get" />
                <input type="text" placeholder="vin" name="vin" id="vin" /><br />OR<br />
                <input type="text" placeholder="make" name="make" id="make" />
                <input type="text" placeholder="model" name="model" id="model" />
                <input type="text" placeholder="year" name="mnf_date" id="mnf_date" />
                <br />ADDITIONAL <br />
                <input id="action" type="text" placeholder="mileage" name="mileage" id="mileage" />
                <input type="text" placeholder="state" name="state" id="state" />
                <input type="text" placeholder="phone" name="phone" id="phone" />
                <input type="text" placeholder="email" name="email" id="email" />
                <input id="action" type="hidden" name="action" value="sort_by" />
                <input type="hidden" name="method" value="get" />
                <br />SORT BY <br />
                <select name="sort-preference" id="sort-preference">
                  <option value="price">Price</option>
                  <option value="coverage_duration">Coverage Duration</option>
                  <!-- option value="coverage-amount">Coverage Amount</option -->
                  <option value="miles">Miles Covered</option>
                </select><br />
                <input type="submit" id="get-quote-submit" />
            </form>

            <div class="form-submission-results"></div>
            <a href="#" class="show-more-results">SHOW MORE / LESS </a>
            <div class="form-submission-results-more"></div>

            <?php

                //save quote if we already have a quote id
                if( isset( $_SESSION['user_id'] ) && $_SESSION['user_id'] != '' ) : ?>

                <form action="/form/" method="post">
                    <h3>PAYMENT INFORMATION</h3>
                    <input type="hidden" name="action" id="action" value="submit_payment" />
                    <input type="hidden" name="method" id="method" value="post" />
                    <input type="text" name="cc_number" id="cc_number" value="" placeholder="Credit Card Number" />
                    <select name="cc_type" id="cc_type">
                      <option value="visa">Visa</option>
                      <option value="master_card">Master Card</option>
                      <option value="discover">Discover</option>
                    </select>
                    <h3>Name on Credit Card</h3>
                    <input type="text" name="cc_first_name" id="cc_first_name" value="" placeholder="First Name on Credit Card" />
                    <input type="text" name="cc_last_name" id="cc_last_name" value="" placeholder="Last Name on Credit Card" />
                    <input type="text" name="cc_csc_code" id="cc_csc_code" value="" placeholder="CSC Code" />
                    <input type="submit" id="submit-payment" />
                </form>

            <?php endif; ?>

            <div class="confirm-order"></div>

		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
