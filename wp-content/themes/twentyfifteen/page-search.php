<?php

    /**
     * The search page template file
     *
     * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
     *
     * @package WordPress
     * @subpackage Twenty_Fifteen
     * @since Twenty Fifteen 1.0
     */
    session_start();
    get_header();

?>

	<div id="primary" class="content-area">

		<main id="main" class="site-main" role="main">

      <h3>Search For a Contract by Parameter</h3>

      <form id="search-form">

        <input type="hidden" name="action" id="action" value="search" />
        <input type="hidden" name="method" id="method" value="get" />
        <input type="text" id="filter-minimum-mileage" name="filter-minimum-mileage" placeholder="Minimum Mileage" />
        <input type="text" id="filter-maximum-mileage" name="filter-maximum-mileage" placeholder="Maximum Mileage" />
        <input type="text" id="filter-maximum-price" name="filter-maximum-price" placeholder="Maximum Price" />
        <input type="submit" id="submit-search" />

     </form>

      <div class="search-results"></div>

    </main><!-- .site-main -->

  </div><!-- .content-area -->

<?php get_footer(); ?>
