<?php

  /**
   * The account page template file
   *
   * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
   *
   * @package WordPress
   * @subpackage Twenty_Fifteen
   * @since Twenty Fifteen 1.0
   */
  session_start();
  get_header();
  $rc = new RestClient();
  //$account_info = json_decode( $rc->get_account_info( $_SESSION['user_id'] ) );

  //error_log( serialize( $account_info ) );

  //$vin = 'JH4TB2H26CC000000';
  //echo $vin[ strlen( $vin ) - 8 ];
  //echo $rc->get_year_from_vin( $vin[ strlen( $vin ) - 8 ] );

  $all_plans      =  json_decode( $rc->test_django() );
  $filtered_plans = array();

  foreach( $all_plans as $plan ) {

    if( $plan->plan_term_miles <= 50000 ) {

      $filtered_plans[] = $plan;

    }//end if

  }//end foreach

  var_dump( $filtered_plans );

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

        <h1>Your Account</h2>
        <h3>Username: <?php echo $account_info->username; ?></h3>
        <h3>Your Plans:</h3>
        <ul>
        <?php foreach( $account_info->plans as $plan_id => $plan ) { ?>

              <li>PLAN ID: <?php echo $plan_id; ?>&nbsp;TYPE: <?php echo $plan->type; ?>&nbsp;PRICE: <?php echo $plan->price; ?></li>

        <?php }//end foreach ?>
      </ul>
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
